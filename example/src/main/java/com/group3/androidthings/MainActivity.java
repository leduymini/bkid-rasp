package com.group3.androidthings;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Spinner;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.AdapterView;

import com.group3.androidthings.MVVM.VM.NPNHomeViewModel;
import com.group3.androidthings.MVVM.View.NPNHomeView;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.google.android.things.pio.SpiDevice;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

import static android.content.ContentValues.TAG;
import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity implements NPNHomeView,AdapterView.OnItemSelectedListener {
    private Rc522 mRc522;
    RfidTask mRfidTask;
    private TextView mTagDetectedView;
    private TextView mTagUidView;
    private TextView mTagResultsView;
    private Button button;
    private Spinner spinner1;
    private Button btnSubmit;

    private ToggleButton crtBttn;
    private ToggleButton inoutBttn;

    private Gpio mLedGpioR;
    private Gpio mLedGpioG;
    private Gpio mLedGpioB;
    private Gpio relay;
    private boolean mLedState = true;
    private boolean master = false;
    private int service = 0;

    private SpiDevice spiDevice;
    private Gpio gpioReset;
    private Handler mHandler = new Handler();
    private Handler handler = new Handler();
    private int count = 0;

    private static final String SPI_PORT = "SPI0.0";
    private static final String PIN_RESET = "BCM25";

    String resultsText = "";

    private NPNHomeViewModel mHomeViewModel;

    private Runnable mBlinkRunnable = new Runnable() {
        @Override
        public void run() {
            count = count + 1;
//            redColor();
            if (count < 20) {
                mHandler.postDelayed(mBlinkRunnable, 150);
            } else {
                count = 0;
//                greenColor();
                mRfidTask.execute(); //addingggg
                mHandler.removeCallbacks(this);
            }

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        BlackColor();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        Log.i(TAG, "interrupt error");
                    }
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if(service != 0) {
                                mHandler.removeCallbacks(mBlinkRunnable);
                                mRfidTask = new RfidTask(mRc522);
                                mRfidTask.execute();
                            }
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        mHomeViewModel = new NPNHomeViewModel();
        mHomeViewModel.attach(this, this);
        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.country_arrays, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        PeripheralManager pioService = PeripheralManager.getInstance();
        try {
            String R = "BCM4";
            String G = "BCM5";
            String B = "BCM6";
            String C = "BCM3";
            relay = PeripheralManager.getInstance().openGpio(C);
            relay.setDirection(Gpio.DIRECTION_OUT_INITIALLY_HIGH);
            mLedGpioR = PeripheralManager.getInstance().openGpio(R);
            mLedGpioR.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            mLedGpioG = PeripheralManager.getInstance().openGpio(G);
            mLedGpioG.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            mLedGpioB = PeripheralManager.getInstance().openGpio(B);
            mLedGpioB.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            spiDevice = pioService.openSpiDevice(SPI_PORT);
            gpioReset = pioService.openGpio(PIN_RESET);
            mRc522 = new Rc522(spiDevice, gpioReset);
            mRc522.setDebugging(true);
        } catch (IOException e) {
            Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
        Button button = findViewById(R.id.button);
        switch (position) {
            case 0:
                service = 0;
                button.setText("Waiting");
                break;

            case 1:
                service = 1;
                yellowColor();
                button.setText("Creating");
                button.setBackgroundColor(Color.RED);
                button.setTextColor(Color.WHITE);
                mHandler.removeCallbacks(mBlinkRunnable);
                mRfidTask = new RfidTask(mRc522);
                mRfidTask.execute();
                break;

            case 2:
                service = 2;
                yellowColor();
                button.setText("Checking");
                button.setBackgroundColor(Color.parseColor("#45b7fc"));
                button.setTextColor(Color.WHITE);
                mHandler.removeCallbacks(mBlinkRunnable);
                mRfidTask = new RfidTask(mRc522);
                mRfidTask.execute();
                break;

            case 3:
                service = 3;
                mHandler.removeCallbacks(mBlinkRunnable);
                System.out.print("Canteen Service");
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (spiDevice != null) {
                spiDevice.close();
            }
            if (gpioReset != null) {
                gpioReset.close();
            }
            mLedGpioR.close();
            mLedGpioG.close();
            mLedGpioB.close();
            mHandler.removeCallbacks(mBlinkRunnable);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            mLedGpioR = null;
            mLedGpioG = null;
            mLedGpioB = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(service != 0) {
            mHandler.removeCallbacks(mBlinkRunnable);
            mRfidTask = new RfidTask(mRc522);
            mRfidTask.execute();
        }
    }

    private void cyanColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Cyan error");
            return;
        }
        try {
            mLedGpioB.setValue(false);
            mLedGpioG.setValue(false);
            mLedGpioR.setValue(true);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void purpleColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Purple error");
            return;
        }
        try {
            mLedGpioB.setValue(false);
            mLedGpioG.setValue(true);
            mLedGpioR.setValue(false);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void yellowColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Yellow error");
            return;
        }
        try {
            mLedGpioB.setValue(true);
            mLedGpioG.setValue(false);
            mLedGpioR.setValue(false);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void redColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Red error");
            return;
        }
        try {
            mLedState = !mLedState;
            mLedGpioR.setValue(mLedState);
//            mLedGpioR.setValue(false);
            mLedGpioG.setValue(true);
            mLedGpioB.setValue(true);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void blueColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Blue error");
            return;
        }
        try {
            mLedGpioB.setValue(false);
            mLedGpioG.setValue(true);
            mLedGpioR.setValue(true);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void greenColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Green error");
            return;
        }
        try {
            mLedGpioG.setValue(false);
            mLedGpioR.setValue(true);
            mLedGpioB.setValue(true);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void whiteColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "White error");
            return;
        }
        try {
            mLedGpioG.setValue(false);
            mLedGpioR.setValue(false);
            mLedGpioB.setValue(false);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    private void BlackColor() {
        if (mLedGpioR == null) {
            Log.d("LED", "Black error");
            return;
        }
        try {
            mLedGpioG.setValue(true);
            mLedGpioR.setValue(true);
            mLedGpioB.setValue(true);
        } catch (IOException e) {
            Log.e("Error", "Error on PeripheralIO API", e);
        }
    }

    @Override
    public void onSuccessUpdateServer(JSONObject response) {
        try {
            if(service == 1) {
                if(!master) {
                    resultsText = response.getString("master");
                    if(resultsText.equals("true")) {
                        master = true;
                        purpleColor();
                    }
                    if(resultsText.equals("false")) {
                        mLedState = true;
                        for(int i = 0; i<5; i++) {
                            redColor();
                            try {
                                sleep(250);
                            } catch (Exception e) {
                                System.out.print(e);
                            }
                        }
                        yellowColor();
                    }
                }
                else {
                    resultsText = response.getString("status");
                    if(resultsText.equals("true")) {
                        master = false;
                        greenColor();
                        try {
                            sleep(2000);
                        } catch (Exception e) {
                            System.out.print(e);
                        }
                        yellowColor();
                    }
                    else {
                        mLedState = true;
                        for(int i = 0; i<5; i++) {
                            redColor();
                            try {
                                sleep(250);
                            } catch (Exception e) {
                                System.out.print(e);
                            }
                        }
                        yellowColor();
                    }
                }
            }
            else if(service == 2) {
                resultsText = response.getString("check");
                if(resultsText.equals("true")) {
                    relay.setValue(false);
                    blueColor();
                    try {
                        sleep(2000);
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                    yellowColor();
                    relay.setValue(true);
                }
                else{
                    mLedState = true;
                    for(int i = 0; i<5; i++) {
                        redColor();
                        try {
                            sleep(250);
                        } catch (Exception e) {
                            System.out.print(e);
                        }
                    }
                    yellowColor();
                }
            }
            else if(service == 3) {
                cyanColor();
            }
        } catch (Exception e) {
            System.out.print(e);
        }
    }

        @Override
        public void onErrorUpdateServer (String message){
            System.out.println(message);
        }

        private class RfidTask extends AsyncTask<Object, Object, Boolean> {
            private static final String TAG = "RfidTask";
            private Rc522 rc522;

            RfidTask(Rc522 rc522) {
                this.rc522 = rc522;
            }

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected Boolean doInBackground(Object... params) {
                rc522.stopCrypto();
                while (true) {
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return null;
                    }
                    //Check if a RFID tag has been found
                    if (!rc522.request()) {
                        continue;
                    }
                    //Check for collision errors
                    if (!rc522.antiCollisionDetect()) {
                        continue;
                    }
                    byte[] uuid = rc522.getUid();
//                    yellowColor();
                    return rc522.selectTag(uuid);
                }
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (!success) {
//                    mTagResultsView.setText(R.string.unknown_error);
                    return;
                }
                // Try to avoid doing any non RC522 operations until you're done communicating with it.
                byte address = Rc522.getBlockAddress(2, 1);
                // Mifare's card default key A and key B, the key may have been changed previously
                byte[] key = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
                // Each sector holds 16 bytes
                // In this case, Rc522.AUTH_A or Rc522.AUTH_B can be used
                try {
                    //We need to authenticate the card, each sector can have a different key
                    boolean result = rc522.authenticateCard(Rc522.AUTH_A, address, key);
                    if (!result) {
//                        mTagResultsView.setText(R.string.authetication_error);
                        return;
                    }

                    Log.d("ReadData", rc522.getUidString());
//                    Date date = new Date();
//
//                    long time = date.getTime();
//                    System.out.println("Time in Milliseconds: " + time);
//
                    if(service == 1) {
                        if(!master){
                            mHomeViewModel.updateToServer("https://bk-id.herokuapp.com/cards/check/master/UID/" + rc522.getUidString());
                        }
                        else {
                            mHomeViewModel.updateToServer("https://bk-id.herokuapp.com/cards/issue/blank/UID/" + rc522.getUidString());
                        }
                    }

                    if(service == 2) {
                        mHomeViewModel.updateToServer("https://bk-id.herokuapp.com/services/check/" + rc522.getUidString());
                    }
                    rc522.stopCrypto();
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
            }
        }
    }
